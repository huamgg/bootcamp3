import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  constructor() { }

   roomList : Object[] =[
    {"id":"1", "name":"Family room", "price":"999.900", "img":"/assets/img/family.jpg", "desc": "this is available",
      "stockroom" : 30, "duration" : 0},
    {"id":"2", "name":"Super room", "price":"1.699.900", "img":"/assets/img/super.jpg", "desc": "this is available",
      "stockroom" : 15, "duration" : 0},
    {"id":"3", "name":"Luxury room", "price":"1.899.900", "img":"/assets/img/luxury.jpg", "desc": "this is available",
      "stockroom" : 0, "duration" : 0},
  ];


  
   


  ngOnInit() {
  }

}
