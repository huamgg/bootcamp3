import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoomListComponent } from './room-list/room-list.component';
import { TipeRoomComponent } from './tipe-room/tipe-room.component';
import { CustomerComponent } from './customer/customer.component';
import { ReservasiComponent } from './reservasi/reservasi.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomListComponent,
    TipeRoomComponent,
    CustomerComponent,
    ReservasiComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
