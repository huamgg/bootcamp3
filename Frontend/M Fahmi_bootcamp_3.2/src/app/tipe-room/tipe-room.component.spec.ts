import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipeRoomComponent } from './tipe-room.component';

describe('TipeRoomComponent', () => {
  let component: TipeRoomComponent;
  let fixture: ComponentFixture<TipeRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipeRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipeRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
