<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_rooms', function (Blueprint $table) {
            $table->increments('list_id');
            $table->string('no_kamar');
            $table->string('status_kamar');

            $table->integer('room_id')->unsigned();
            $table->foreign('room_id')->references('room_id')->on('tipe_rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_rooms');
    }
}
