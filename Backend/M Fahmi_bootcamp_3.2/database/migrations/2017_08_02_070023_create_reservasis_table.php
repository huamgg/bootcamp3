<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservasis', function (Blueprint $table) {
            $table->increments('customer_id');
            $table->date('check_in');
            $table->date('check_out');
            $table->decimal('total_price');

            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('customer_id')->on('customers');
            $table->integer('list_id')->unsigned();
            $table->foreign('list_id')->references('list_id')->on('list_rooms');
            $table->integer('room_id')->unsigned();
            $table->foreign('room_id')->references('room_id')->on('tipe_rooms');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservasis');
    }
}
